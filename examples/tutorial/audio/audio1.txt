$ global name=audio1
$ scene1 visualization=../TransAtlas.vmd

# scene1

! External audio can be added to videos e.g. from
! an .mp3 file if one specifies the file, starting
! time (default is 0), and length (default is until 
! next audio track or until the end)

translate   vector=-1.25,0,0
{rotate     axis=y angle=3600 t=10s sigmoid=f;
add_audio   audiofile=rick.mp3;
translate   vector=2.6,0,0}
translate   vector=-1.35,0,0
zoom_out    scale=40
make_transparent material=Diffuse
{zoom_in    scale=40 t=8s;
highlight   selection=all color=structure mode=u;
add_audio   audiofile=rick.mp3 from=127;
rotate      axis=y angle=-7200 fraction=0.5:}
do_nothing  t=2s
