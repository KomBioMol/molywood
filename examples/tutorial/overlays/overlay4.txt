$ global fps=6 name=overlay4 draft=t
$ scene1 visualization=../TransAtlas.vmd 

# scene1

! data from 'datafile' will be plotted alongside the animation
! when 'add_overlay' is combined with 'animate'; axes labels
! can be defined in the pca.dat file as comments

{animate     frames=0:17 t=3s;
 add_overlay datafile=pca3.dat relative_size=0.4 origin=0.6,0.6 aspect_ratio=1 2D=t}
{animate     frames=17:0 t=3s;
 add_overlay datafile=pca4.dat relative_size=0.4 origin=0.0,0.0 aspect_ratio=1 2D=t}
