# Features to include in future releases:

* add cooler intro movie
  * differentiate between rendered and non-rendered
  * make it simple but highlight strengths
 
* support for 2D- and 3D-visualization with e.g. PCA data (kernel density estimates?)
  * document and test options for multiple ensembles (like apo and bound)
  * add a standardized option to plot 2D free energy plots

* tutorials and tests for new features
  * document multiple 1D plots as overlays
 
* a possibility to switch the top molecule in VMD
  * add docs and tutorials for `toggle_molecule`
  * add an optional parameter `molecule_id` to some actions to allow simultaneous mods of many mols

* allow for saving viewpoints and returning to them smoothly
  * add `save_view` and `retrieve_view` with aliases

* fix instantaneous transparencies
  * make `make_transparent` more intuitive

* (ambitious) selected Pov-Ray features, e.g. density slicing
  * try handling density slicing through a plugin, switch all to pov-ray if needed?

* (super-ambitious) produce nicer labels
  * problematic: render everything-but-labels with Snapshot, then locate origin?
  & re-render text with Python? (how to handle overlaps?)
  
* (hyper-ambitious) provide support for PyMOL or/and UCSF Chimera
  * would require a complete rewrite of a copy of `tcl_actions.py`, as well as 
  a slight modularization of `moly.py` (e.g. a separate Visualizer class?)
