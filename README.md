# molywood-gui



## What is Molywood?

Molywood is a standalone Python library developed at IRB Barcelona to accelerate movie-making in VMD, thoroughly described and documented [here](https://mmb.irbbarcelona.org/molywood/).

## What does this GUI do?

This TCL graphical interface brings advanced movie-making to your desktop version of VMD, starting from VMD 2.0. Its advantages include:

    - dropdown menus with all available actions and parameters: no need to learn the syntax
    - integrated previews: no need to restart VMD each time you want to preview the movie
    - pre-packaged dependencies: Molywood and other required libraries are shipped together with VMD

## Can I request new features?

A large part of Molywood's development has been driven by users' feedback. If you have ideas for missing features, let us know! To suggest modifications, ask for guidance or report undesired behavior, get in touch with Miłosz Wieczór at milosz.wieczor@irbbarcelona.org.