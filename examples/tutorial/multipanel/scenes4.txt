$ global name=scenes4
$ scene1 pdb_code=1w0t,1w0u

# scene1

! In the same way, `toggle_molecule` can be used
! to change the top molecule in VMD, thus selecting
! which molecule e.g. the highlight will be applied to;
! from v 0.2,`fit_trajectory` can be used with the
! `molecules` parameter to only affect selected molecules

rotate           axis=x angle=90
translate        vector=0.4,0,0 t=1s
toggle_molecule  molecule_id=1 freeze=t
translate        vector=-0.3,-1.8,0 t=1s
fit_trajectory   selection=nucleic axis=z t=1s molecules=0
fit_trajectory   selection=nucleic axis=z t=1s molecules=1
toggle_molecule  molecule_id=1 freeze=f

toggle_molecule  molecule_id=0 freeze=t
rotate           axis=y angle=-165 t=1s
toggle_molecule  molecule_id=0 freeze=f

toggle_molecule  molecule_id=0 top=t
highlight        selection='resid 418 to 430' style=licorice color=type t=4s
toggle_molecule  molecule_id=1 top=t
highlight        selection='resid 484 to 500' style=licorice color=type t=4s
