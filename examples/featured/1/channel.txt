$ global draft=f keepframes=f fps=20 name=amb render=t 
$ scene1 resolution=900,1440 visualization=transfer.vmd

    ! At first, representations were set up in VMD, and color scales
    ! were defined so that the passing ion changes color from blue
    ! to green; visualization state was saved to transfer.vmd

# scene1

    ! To make the animation smoother, 'fit_trajectory' aligns the selected
    ! atoms across all frames (with respect to the first frame):

fit_trajectory    selection='resname AMFB'

    ! If the system moves away from the center, a simple instantaneous
    ! 'translate' can bring it back into the field of view

translate         vector=0,0.2,0

    ! 'animate' can be used to jump to a different frame than default last one

animate           frames=1

    ! 'add_overlay' puts the (partially transparent) figure atop the movie;
    ! it was produced so as to have the same resolution as the movie itself
    ! ('keepframes=t' can be useful to align things with individual frames)

{
 add_overlay      figure=label.png relative_size=1 sigmoid=t;
 do_nothing       t=3s
}

    ! we use 'center_view' to ensure that the upcoming 'zoom_in' will head
    ! towards the channel molecules

center_view       selection='resname AMFB'
zoom_in           scale=1.4 t=3s

    ! here 'highlight' creates a new representation of the entire channel, while
    ! 'make_transparent' simultaneously removes all Opaque elements to clean the view

{
 highlight        selection='resname AMFB and noh' mode=u color=type style=vdw alias=channel t=3s;
 make_transparent material=Opaque
}

    ! a series of 'rotate' actions gives a good sense of the 3D structure

rotate            axis=x angle=90 t=2s
rotate            axis=x angle=-90 t=2s

    ! 'make_opaque' is an action opposite to 'make_transparent', bringing back the
    ! previously removed elements; simultaneously, 'highlight' with 'mode=d'
    ! and 'alias=channel' gradually removes the representation defined above

{
 make_opaque      material=Opaque t=3s;
 highlight        alias=channel mode=d
}

    ! 'animate' shows 100 frames over 6 seconds, with a smoothing window of 5 frames

animate           frames=1:100 t=6s smooth=5

    ! the last 67 frames are played while simultaneously zooming out

{
 animate          frames=100:167 t=4s smooth=5;
 zoom_out         scale=1.4
}
