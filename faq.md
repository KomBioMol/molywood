#### What can Molywood do for you?

<details>

<summary>Expand/Hide</summary>

Molywood makes molecular movies. Or - at least! - attempts to 
make moviemaking fun. The main goal is to shift the user's
focus from technical to conceptual matters: instead of scratching 
your head over how to make a custom smooth rotation via
a TCL loop, you should be planning sequences and transitions, 
adding suspense and effects, so that your audience (and even 
the reviewers!) will marvel at your professional production.

In the past, every several months we'd go back to the old 
patchy scripts, hopelessly trying to reuse as much as possible
from the previous movies we'd made, only to find that any 
small change required another extra hour of trials and errors.
Simultaneously, when reading a structural paper, we'd often 
wonder - how are we supposed to see your point from these
crowded, still 2D images? If only turning them into a movie 
was as easy as creating a publication figure, right?

With Molywood, we hope that's now the case: with little 
training, you can produce simple movies in minutes, and then
easily add layers of complexity whenever required. Look up 
our [Samples](http://mmb.irbbarcelona.org/molywood/samples) 
and [Gallery](http://mmb.irbbarcelona.org/molywood/gallery), 
walk through the [Tutorials](http://mmb.irbbarcelona.org/molywood/tutorials) 
and start making movies: it should always have been as easy as that.

</details>

#### What is a Molywood project?

<details>

<summary>Expand/Hide</summary>

A Molywood project - or a **Script**, drawing from the filmmaking 
lingo - is a recipe for a movie, defined within a single 
input text file. The file first specifies a number of global 
parameters, such as movie quality, rendering mode, and unique 
characteristics of each **Scene**. Then, each **Scene** is defined in 
detail as a list of individual **Actions** - atomic commands that 
specify duration, extent and properties of each change occurring. 
You can generate a sample input by running `molywood` from the 
command line and choosing `i` when prompted to.

On the technical level, a project is executed in several stages: 
first, each **Scene** is converted to a TCL script, executed by VMD to 
render individual frames. Python's Matplotlib and/or Seaborn 
generate dynamic graphs if needed, and Imagemagick tools are used 
to crop, resize, combine and compose individual graphics as 
required. Once processing is done, FFMpeg combines frames (and 
audio, if needed) into the final movie. However, all this should 
happen without the user knowing: we wanted to hide the bolts and 
nuts to make the script accessible to everyone, even with no 
programming background.

As a visual guide, the structure of the script is illustrated 
[here](https://gitlab.com/KomBioMol/molywood/-/blob/master/script_structure.svg).

</details>

#### What is an Action?

<details>

<summary>Expand/Hide</summary>

In Molywood, an **Action** is a representation of a single event or 
transition in the movie - such as a rotation, trajectory animation, 
addition of labels or change in transparency. They correspond to 
a single line in the input, and are composed of a keyword followed 
by a number of parameters. When required, though, this 'atomistic' 
view of Action can be expanded, for example:

  + finite-time **Actions** can be combined to become simultaneous, to 
  e.g. rotation and zoom happening at the same time;
  + certain **Actions** can be made to represent only a part of the 
  transition using `fraction=begin:end` - these include rotations, 
  translations, zoom and transparency changes;
  + some **Actions** will have a lasting effect on the outcome, e.g. 
  a highlight with `mode=u` will remain visible after the **Action** 
  was completed;
  + some **Actions** can affect previous ones, e.g. a highlight with 
  `mode=d` will remove a previously created representation, given 
  their `alias=...` values match.

</details>

#### What is a Scene?

<details>

<summary>Expand/Hide</summary>

A **Scene** refers to a continuous sequence representing a single 
molecular system, similar to a single "cut" in a movie, and 
is composed of multiple **Actions**. Most simple movies will only 
feature a single **Scene**. However, to foster users' creativity, 
we made it possible to automatically render and compose multiple 
**Scenes**, including the following cases:

  + in a temporal succession - one **Scene** begins where another 
  ends (using `after=...`);
  + as an array - multiple **Scenes** are displayed simultaneously, 
  creating a grid (using `layout` and `position=...`);
  + layered atop of one another - one scene is displayed as an 
  inset on another, e.g. to highlight a chosen feature or 
  contrast with an counterexample (using `add_overlay scene=...`).

Importantly, every **Scene** can be independently assigned a different 
molecular system, a different resolution, and different rendering 
parameters. Each **Scene** has to be defined in the input with a 
`$ scene_name scene_parameters` line, and a corresponding 
`# scene_name` line beginning the list of **Actions** this **Scene** will 
hold. Typical use cases of multi-**Scene** movies can be found on 
the [Samples](http://mmb.irbbarcelona.org/molywood/samples) 
page, in the Multiple Scenes and Overlays sections.

</details>

#### How to efficiently design a movie?

<details>

<summary>Expand/Hide</summary>

First of all, list out all the features you would like to showcase. 
If it's a specific contact or structural element, run VMD and make 
sure you can find a suitable selection. If it's a segment of the 
trajectory, write down the specific range of frames. Look at our 
Gallery and try to decide on specific colors and representations 
for individual features.

Then, begin with a minimal working example: set the global option 
`render=f` to avoid writing frames to disk, make a single 
`do_nothing t=1s` **Action**, and start adding new short **Actions** 
(`t=1s` for each) to immediately see the effect of the change. 
This is useful to play with different angle ranges, zoom values, 
translations and all other numerals that will need visual 
inspection. Once you've arrived at something satisfactory, 
adjust the details: **Action** durations, color settings etc. Try 
switching from `render=f` to `fps=5 draft=t render=t` and see 
if the draft .mp4 file looks fine. If you want to use multiple 
scenes, you might want to repeat this process for each of them.

With `render=t`, you can start adding overlays such as text labels 
or graphs: by using very low fps settings, you may ensure any 
change is visible in a matter of seconds. When you're happy with 
the result, re-render with e.g. `fps=10 draft=t render=t` to 
make sure everything is smooth.

To produce a final version, turn off the draft mode with `draft=f` 
and render with the final fps rate (20-24 will be fine); this 
will now take a while, from several minutes to an hour or more. 
Depending on the movie's complexity, you might want to offload the 
rendering step to a remote workstation. In any case, setting 
`keepframes=t` might be useful - if you decide to only change a 
small detail later on, it might suffice to remove the faulty frames 
and re-run Molywood with `restart=t` to only render the missing frames.

</details>

#### How to create a custom VMD visualization?

<details>

<summary>Expand/Hide</summary>

To add custom representations as a starting point for the movie, 
open your system of interest in VMD and use the available tools 
to give it a desired look and set a proper camera angle. Then in 
the VMD Main window, go to File > Save Visualization State and 
save the current state to a .vmd file. To use this state as a 
starting point for a **Scene**, set the **Scene** parameter 
`visualization=...` in the text input to point to that .vmd file.

Note: not all VMD features can be written to and retrieved from 
a .vmd file; in particular, RMSD-based alignment, color scale 
changes and the effects of most plugins are not recorded in this 
way. Some of these are available as Molywood **Actions** (e.g. 
trajectory alignment as `fit_trajectory`), but others are 
currently not supported.

</details>

#### How to add an interactive data plot to my movie?

<details>

<summary>Expand/Hide</summary>

The easiest way is to have a trajectory consistent with the data 
file, so that there is a one-to-one correspondence between 
consecutive trajectory frames and data points. If you have an 
`animate` **Action** in your **Scene**, use curly brackets to combine 
it with an `add_overlay` **Action**, providing the latter with a 
`datafile=...` parameter. To position the overlay (plot) on 
the **Scene**, use `relative_size=...` (a value between 0 and 1) 
and `origin=x0,y0`, where x0 and y0 denote the relative location 
of the bottom left corner. (Example: `relative_size=0.5 
origin=0.5,0.5` will yield an overlay covering the upper 
right quarter of the **Scene**.)

To modify the plot, you can set an `aspect_ratio=...` different 
from 1, or add `transparent_background=t` to make all white 
plot elements transparent. Also, if the dot should move 
independently from the `animate` frames, use `dataframes=initial:final` 
to specify a range of data points from the datafile to be used.

To label the axes, you can add a header to your datafile 
(`# x-label; y-label`), possibly using LaTeX-compatible expressions 
encircled with dollar signs. Matplotlib-specific plot details can 
also be changed with a different header, e.g. 
`! color='r' ylim=[-2,2] ls='--'` will produce a red dashed line, 
restricting the Y axis to a range from -2 to 2. For more 
examples, consult Overlays in [Samples](http://mmb.irbbarcelona.org/molywood/samples), 
along with the .dat [source files](https://gitlab.com/KomBioMol/molywood/-/tree/master/examples/tutorial/overlays) 
hosted on GitLab.

</details>
