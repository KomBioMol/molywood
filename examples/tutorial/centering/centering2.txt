$ global name=centering2
$ scene1 visualization=../TransAtlas.vmd 

# scene1

! when rotating, center_view can change the pivot, effectively
! achieving a result identical to a combined rotation + translation

center_view       selection='resid 105 to 235 or resid 265 to 275'
highlight         selection='resid 105 to 235 or resid 265 to 275' material=Diffuse color=structure mode=u alias=h1
make_transparent  material=Diffuse limit=0.4 t=1s
rotate            axis=x angle=90 t=2s
make_opaque       material=Diffuse start=0.4 limit=1 t=1s
highlight         alias=h1 mode=d

center_view       selection='not (resid 105 to 235 or resid 265 to 275)'
highlight         selection='not (resid 105 to 235 or resid 265 to 275)' material=diffuse color=structure mode=u alias=h1
make_transparent  material=Diffuse limit=0.4 t=1s
rotate            axis=x angle=90 t=2s
make_opaque       material=Diffuse start=0.4 limit=1 t=1s
