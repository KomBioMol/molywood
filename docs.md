## Table of contents

- [Input structure](#input-structure)
  - [Notes on input structure](#notes-input)
- [Global keywords](#available-global)
  - [List of available global keywords and parameters](#available-global)
  - [Notes on global keywords](#notes-global)
      - [Scene keywords](#notes-scene)
      - [Loops](#loops)
- [Actions](#available-keywords)
  - [List of available action keywords and parameters](#available-keywords)
      - [Instantaneous actions](#available-instantaneous)
      - [Instantaneous or finite-time actions](#available-iof)
      - [Finite-time actions](#available-finite)
  - [Notes on individual actions](#notes-actions)
      - [Instantaneous actions](#notes-instantaneous)
      - [Instantaneous or finite-time actions](#notes-iof)
      - [Finite-time actions](#notes-finite)
- [Molywood GUI in VMD](#gui)
- [Known issues](#issues)
- [Advanced tips](#tips)

<a name="input-structure"></a>
### Input structure

Input files should be structured according to the general template:

```
$ global_keyword1 key1=value1 key2=value2 ...
$ global_keyword2 key1=value1 key2=value2 ...
$ ...

# scene1_name
action1 key1=value1 key2=value2 ...    ! this is a single action
action2 key1=value1 key2=value2 ...    ! this is a single action, too
{                                      ! this requests two concurrent actions
 action3 key1=value1 key2=value2 ...
 action4 key1=value1 key2=value2 ...
}
...

# scene2_name
...
```
<a name="notes-input"></a>
### Notes on input formatting:

+ A line starting with a `$` sign specifies global keywords, e.g.
`$ global fps=20` (see available global keywords below)
+ A hash `#` marks the *beginning* of a scene input section, and should
be followed by a single-word scene identifier,  e.g. `# scene_1`
+ Single actions have to be specified on a single line, starting with
a keyword and followed by any number of whitespace-separated
 `parameter=value` pairs (note: no spaces encircling the `=` sign)
+ Multiple actions (e.g. rotation and zoom_in) can be performed
simultaneously if they are encircled in curly brackets and separated
with semicolons, e.g. `{rotate t=1s angle=50; zoom_in scale=2}`;
they can also be split over several lines for convenience. Note that the
`t=...s` parameter only has to be specified once per curly bracket
+ true/false values can be specified as `true/false`, `yes/no` or in
shorthand notation (`t/f`, `y/n`)
+ Comments can be introduced with an exclamation mark, `!`

<a name="available-global"></a>
### List of available global keywords and parameters:

+ global (\[fps=**20**  draft=t/**f** keepframes=t/**f** render=**t**/f
restart=t/**f** name=**movie** breakpoints=... gif=**8**,**400**\])
+ layout (\[rows=**1** columns=**1**\])
+ master_overlay (t=...s start_time=...  \[figure=... datafile=... origin=0,0 text=...
relative_size=**1** dataframes=init_frame:final_frame 
dataframes_from_file=filename.dat aspect_ratio=...
2D=t/**f** textsize=**1** sigmoid=t/**f** alpha=a_init\[:a_final\]
scene=... transparent_background=t/**f** textcolor=**black**
decimal_points=**3** movie=...\])
+ scene_identifier (\[visualization=... structure=... trajectory=...
pdb_code=... position=**0,0** resolution=**1000,1000** after=...
ambient_occlusion=t/**f** max_transparent_surfaces=**3** 
variables=key1:value1,key2:value2,... render_only=...\])

(instead of scene_identifier, you should put the actual identifier
of the scene in question, e.g. `scene_1` in the example below)

<a name="notes-global"></a>
### Notes on global keywords:

+ `fps=...` sets the frame ratio, effectively translating `t=...s`
keywords in indivivual actions into actual frame numbers. A value of
15-25 will produce a smooth movie; setting it low (2-10) is, however,
convenient for early tests and experimentation as the movie preparation
becomes very fast.
+ The `draft` mode allows for a quick-and-dirty render on a local
machine, using the Snapshot renderer (esentially VMD screenshots)
instead of the more robust but costly Tachyon. In general, one should
 not run a `draft=f` job until a `draft=t` movie looks good. Note: minor
aspects (e.g. the appearence of transparent materials or text labels)
will differ between the two modes.
+ With `keepframes`, one can preserve most of the files generated
on-the-fly (.png frames and the .tcl script). This can be useful in
many cases, e.g. when designing complex overlays on top of already
rendered frames, or when introducing custom changes into the .tcl script.
This will also allow for fast re-rendering with only very small changes,
by removing the affected frames and setting `restart=t` (see below).
+ The `render` option indicates that the run should render frames and
produce a movie. Turning it off (`render=f`) allows the user to only
"play" the movie within VMD (consecutive actions are performed but no
image files are stored), so that the disk drive is not overwhelmed,
but scene composition and overlays cannot be previewed this way -
use `draft=t` instead to preview the final result.
+ `restart=t` can be used if a fraction of frames has already been
successfully rendered (sometimes taking a lot of time) and only the
remaining ones should be produced. In this case, the script should
automatically detect which actions have been rendered completely, and
only re-render missing ones.
+ `name` only affects the names of the .png frames and the final
.mp4 movie file. Can be useful to avoid overwriting existing files.
+ `layout` only needs to be specified when creating a grid of scenes.
In that case, the number of `rows` and/or `columns` has to be defined,
and `position` has to be specified for each scene (see below).
+ `master_overlay` specifies an overlay (conceptually identical to 
`add_overlay` documented below) that is added on top of the combined
scenes, and can be useful for overlaying e.g. a label or a legend on a 
grid of scenes defined with `layout` (where `add_overlay` used **within** 
that scene would only affect one piece of the grid, not the entire movie).
Accepted parameters are identical to `add_overlay`, except `alias` and
`mode`; `t=...s` specifies duration, and compulsory `start_time=...`
specifies the onset of the overlay (in movie's absolute time, in seconds);
both can be floating point numbers if needed.


<a name="notes-scene"></a>
##### Scene keywords
+ `visualization=file.vmd` indicates a .vmd visualization state file,
generated in VMD through `File>Save Visualization State`. It is a 
TCL script containing all commands necessary to reproduce the saved view.
Please keep in mind that not all VMD operations are properly saved
in the visualization state; examples inlude RMSD fitting (try working
with `fit_trajectory` to reproduce the desired effect) or the setting of
clipping planes.
+ `structure=file.pdb` indicates a structure file with a VMD-compatible extension
(most currently used formats, from .pdb to .gro to .xyz or .mol2).
If used instead of `visualization`, the structure will be read and
displayed using the default style (NewCartoon and secondary structure-based
coloring).
+ `trajectory=file.xtc` indicates a compressed trajectory file that has to
be loaded alongside `structure`. While the `visualizaton` option will
typically be preferred to the `structure` + `trajectory` combination,
the latter might come in handy when e.g. automatizing movie production
on a remote machine.
+ `pdb_code=1abc` specifies a 4-character code from the PDB database
(www.rcsb.org). The corresponding entry will be downloaded and visualized
using the default style (NewCartoon and secondary structure-based
coloring).
+ `cubes=filename_pattern*.cube` specifies a regular expression that will
be used to select all .cube files matching it, and load them (ordered by
numeric values in the name) as a trajectory with dynamic densities; this
is useful for working with AIMD or QM/MM trajectories. `highlight` can
be combined with `isovalue` to add 3D density visualization.
+ `position=row,column` specifies the location of the given scene on 
a grid, when a grid of scenes is enabled with the `layout` keyword
(see above). It requires two integers, the first speficying the row
and second the column. Note that it is the user who is responsible for
matching the resolutions of individual scenes.
+ `resolution=X,Y` specifies the scene's resolution (in pixels). Note that
(1) in the draft mode, resolution cannot be larger than screen size; (2) 
with Tachyon rendering, higher resolutions will take significantly longer
to render, so try using a low value if you want to test your settings
before the final rendering.
+ `after=other_scene_name` allows to join two scenes in time, so that the current
scene will be appended to the end of `other_scene_name`.
+ `ambient_occlusion=t` enables the Tachyon's Ambient Occlusion
rendering mode (with a predefined setting). This yields the most
visually appealing results, but will take significantly (at least order
of magnitude) longer, so it should only be applied as the final polish
to already well-tested setups. Not compatible with the draft mode.
(Tip: Using the ambient occlusion-optimized AOEdgy, AOChalky, AOShiny
materials might produce optimal results with this option.)
+ `max_transparent_surfaces=...` sets the maximum number of transparencies
allowed by the renderer. When set to the default (`...=1`), only one object
on top will appear as transparent; when set to a large number, all
elements will maintain transparency, which might produce ugly results in
certain setups, e.g., with semi-transparent Licorice or NewCartoon
representations.
+ `variables=key1:value1,key2:value2,...` allows to define per-scene variables
that can be later used in Action definitions by placing `<key1>` keywords where
`value1` should be placed; this way one can e.g. use the concise `# scene1,scene2,...`
syntax for multiple scenes when action durations or parameters differ between scenes.
+ `render_only=...` allows the user to only render (or re-render) selected frames
by listing a set of frame numbers; other commands will still be performed but
rendering will be skipped. Frames can be listed as comma-separated integers
or ranges defined with dashes (inclusive), so `render_only=7,8,10-12,15` will render
frames 7, 8, 10, 11, 12 and 15. Note that in this case most postprocessing actions
(such as composing and producing overlays) will not be performed. Also note that
changing the `fps` parameter will change which frames are rendered.

<a name="notes-scene"></a>
##### Loops
If a part of the code has to be repeated multiple times with different input values,
one can use the following syntax:
```
repeat var1 var2 [var1_value1 var1_value2] [var2_value1 var2_value2]
some_action param1=<var1> t=<var2>s
end repeat 
```
In the above case, `some_action` will be performed twice with different times and parameters.

As processing of loops is performed on the raw input, they can be inserted 
essentially anywhere, not just to generate repeated actions but also 
define multiple scenes or `master_overlay`s.

<a name="available-keywords"></a>
### List of available action keywords and parameters:

Actions in `molywood` can be instantaneous-only (e.g. adding or removing labels),
finite-time-only (e.g. displaying an overlay or figure) or either, depending on
user's choice (rotation, transparency change etc). When both options
are available, the sole presence of the `t=...s` keyword makes an action
finite-time rather than instantaneous.

<a name="available-instantaneous"></a>
##### Instantaneous actions:

Square brackets denote optional
parameters. Values in bold font indicate defaults when parameters
are optional.

+ center_view (selection='...' molecules=**all**)
+ add_label (label='...' atom_index=... \[label_color=**'black'**
text_size=**1** alias=...\])
+ add_distance (selection1='...' selection2='...' \[label_color=**'black'**
text_size=**1** alias=...\])
+ remove_label (\[alias=... all=t/**f**\])
+ remove_distance (\[alias=... all=t/**f**\])
+ save_viewpoint (alias=...)
+ toggle_molecule (molecule_id=... \[top=t/f, freeze=t/f active=t/f drawn=t/f\])
+ show_grid

<a name="available-iof"></a>
##### Instantaneous or finite-time actions

Square brackets denote optional
parameters. Values in bold font indicate defaults when parameters
are optional.

+ fit_trajectory (selection='...' \[axis=x/y/z/"v_x v_y v_z" t=...s
invert=t/**f**\] molecules=**all** selection_ref=... molecule_ref=...)
+ rotate (axis=x/y/z angle=... \[sigmoid=**t**/f/sls t=...s fraction=**0.0:1.0** molecules=...\])
+ translate (vector=Tx,Ty,Tz \[normalize=t/**f** sigmoid=**t**/f/sls t=...s fraction=**0.0:1.0** molecules=...\])
+ zoom_in/zoom_out (scale=... \[sigmoid=**t**/f/sls t=...s fraction=**0.0:1.0**\])
+ animate (frames=init_frame:\[final_frame/last\] \[smooth=**1** t=...s\])
+ make_transparent/make_opaque (material=... \[sigmoid=**t**/f/sls
start=... limit=... t=...s fraction=**0.0:1.0**\])
+ highlight (selection=... \[color=**red** mode=u/d/n/**ud**
material=**Opaque**/Diffuse/Transparent/... isovalue=... inline_parameter=... contour=...
style=**newcartoon**/licorice/surf/quicksurf/vdw/cpk/tube/isosurface/dynamicbonds cutoff=**1.6** 
fade_in=**0.2** fade_out=**0.2** thickness=**1.0** volume_offset=**0** alias=... molecules=**all** t=...s\])
+ restore_viewpoint (alias=... \[sigmoid=**t**/f/sls t=...s fraction=**0.0:1.0**\])
+ insert_tcl (\[code=... file=code.tcl loopover=... range=init_val:final_val loop_command=... t=...s\])

<a name="available-finite"></a>
##### Finite-time actions:

Square brackets denote optional
parameters. Values in bold font indicate defaults when parameters
are optional.

+ show_figure (t=...s \[figure=... datafile=...
dataframes=init_frame:final_frame dataframes_from_file=filename.dat\])
+ do_nothing (t=...s)
+ add_overlay (t=...s 
\[origin=0,0 relative_size=**1** aspect_ratio=... mode=u/d/n/**ud** alias=...
sigmoid=t/**f** alpha=a_init\[:a_final\] transparent_background=t/**f** 
dataframes=init_frame:final_frame datafile=... dataframes_from_file=filename.dat 2D=t/**f** seaborn=**f**
text=... textsize=**1** textcolor=**black** textbox=t/**f** decimal_points=**3** 
scene=... figure=... movie=... center=t/**f**\])
+ add_audio (audiofile=... \[from=**0** length=... volume=**1**\])

Note that **only** finite-time actions can be combined using curly
brackets; instantaneous actions should be executed sequentially, as they
have no duration by definition.

<a name="notes-actions"></a>
### Notes on individual actions

<a name="notes-individual"></a>
##### Instantaneous actions:

+ `center_view` is an instantaneous action that sets the geometric
center of `selection` (VMD-compatible) as the new camera center to
which zoom will converge; useful when zooming onto e.g. a reaction center.
If multiple molecules/systems are loaded in VMD (e.g. for alignment of two
structures), use `molecules=...` to choose which will be affected; default
is `all`, other options include `top` (currently selected) or a comma-separated
list of 0-based integers (e.g. `0,2,3`).
+ `add_label` instantaneously adds a text label anchored to the atom
specified with `atom_index`, with the labeling text specified through
the `label` parameter; if desired, text size  and color can be
specified with `label_color` and `text_size`. If the label will be removed
later, consider giving it an `alias=...`. While VMD labels tend to be
ugly, an obvious advantage of `add_label` over an `add_overlay text=...` directive
is that the label moves in sync with the molecule.
+ `add_distance` instantaneously adds a distance label between
the centers of geometry of two VMD-compatible selections, specified
with `selection1` and `selection2`; as above, text size and color can
be specified with `label_color` and `text_size`, and an `alias=...` can
be set to remove the distance label later on.
+ `remove_label` instantaneously deletes a label specified through
`alias=...` identical to an `alias` previously specified in `add_label`;
alternatively, `all=t` removes all existing labels. Note that `alias=...`
and `all=t` should be mutually exclusive: one either deletes a specific
label or all of them.
+ `remove_distance` works identically to `remove_label`, but affects
labels added using `add_distance` instead of `add_label`.
+ `toggle_molecule` allows to dynamically change molecule properties in VMD:
`freeze`, `drawn` and `active` can be toggled on and off with `t` or `f`,
and the top molecule can be changed by setting `top=t`, all for the molecule
specified with `molecule_id=...`.
+ `save_viewpoint` saves a camera position (rotation, zoom, translation)
with a unique name, allowing to later come back to the exact position by
invoking the initially chosen `alias`
+ `show_grid` generates an additional frame with a 10x10 grid
overlaid on top of the current image; this might be helpful in placement of
overlays that require their position to be specified. The file will be named 
`grid-scene_name-frame_number.png`.

<a name="notes-iof"></a>
##### Instantaneous or finite-time actions

+ `animate` runs the trajectory from `init_frame` to `final_frame`,
adjusting the playback speed to the time specified with `t`;
`smooth=X` sets the smoothing of all VMD representations to X. When `t`
is not specified, `animate frames=Y` instantaneously goes to frame
number `Y`. Note that `last` can be used instead of a frame number to
refer to the last frame in the trajectory.
+ `rotate` rotates the scene by `angle` degrees about `axis` in time `t`.
`sigmoid=t` gives a smooth transition, while `sigmoid=f` gives a
constant-velocity one; optionally, `sigmoid=sls` performs a smooth-
linear-smooth transition (preferable for e.g. multiple full rotations);
when `t` is not specified, `rotate` will behave as an instantaneous
action. `abruptness` controls the rate of the sigmoid transition.
To rotate only a selected subset of molecules/systems in the scene,
use e.g. `molecules=0` for the 0-th molecule, or `molecules=1,2` for
molecules 1 and 2; by default, rotation is applied to all of them
(equivalent to `molecules=all`).
+ `translate` moves the camera position by `vector`, specified by 
three (or two; the third value, depth, will be ignored anyway) 
comma-separated values. If `normalize` is set to `t`, the unit is screen 
size (from center to the edge), so usually fractional values should be 
used to achieve a modest change; without `normalize`, this can be dependent
on such quantities as aspect ratio and screen height. Other parameters (`t`,
`sigmoid`, `abruptness`, `molecule`) behave identically to `rotate`.
+ `zoom_in`/`zoom_out` scales the view by a factor of `scale`. Other
parameters (`t`, `sigmoid`, `abruptness`) behave identically to `rotate`.
+ `fit_trajectory` uses RMSD-based fitting to instantaneously align a
trajectory to the reference (first) frame, using the `selection` to
calculate the optimal alignment; when `t` is specified, this alignment
will be gradual instead of abrupt, with `abruptness` controlling
the rate of transition. When `axis=x/y/z/"x_comp y_comp z_comp"`
is specified, the main principal axis of `selection` will instead be aligned
to the chosen/defined axis. If you want to choose an opposite orientation
than is done by default, simply add `invert=t`. 
    + If multiple molecules/systems are loaded in VMD (e.g. for alignment of
    two structures), use `molecules=...` to choose which will be affected; 
    default is `all`, other options include `top` (currently selected) or 
    a comma-separated list of 0-based integers (e.g. `0,2,3`).
    + Similarly, in the case of multiple molecules being present, alignment
    can be made **between** molecules, by providing the index of `molecule_ref`
    that will be used as a reference, and `selection_ref` if the selection in 
    `molecule_ref` should be different from `selection` in the molecule(s) being
    aligned. By default, `molecule_ref` is same as `molecules`, and `selection_ref`
    is same as `selection`. `frame_ref` can be used to specify the reference frame
    index in the molecule/system specified with `molecule_ref`.
+ `highlight` creates a new representation to highlight a subset of
the system selected by specifying the `selection` parameter. Color can
be chosen with `color`, using either simple color names (black, red,
blue, orange, yellow, green and white), VMD-compatible ColorIDs (0-32)
or a coloring scheme keyword (name, type, element, structure etc.).
Similarly, `style` can be set to newcartoon, licorice, surf,
quicksurf, vdw, dynamicbonds, cpk or tube (non-case sensitive).
    + By default, highlight appears (fades in from transparency) and
    disappears smoothly over the course of the action. If you want your
    highlight to stay visible, use `mode=u` (up) to make it appear only.
    Add an `alias=...` if you want to remove it later (see below).
    + To modify a pre-existing representation (e.g. defined in the 
    visualization state), use `alias=_rep0`, `alias=_rep1` etc. to access
    the first, second, ... representation respectively. This way, one can
    modify the thickness, opacity (`alpha=...`), or isovalue associated
    with that representation. Note that you have to specify `mode=n` as
    other modes are not compatible with this option.
    + To turn off a previously created highlight (possibly after several
    intervening actions), use `mode=d` (down) along with
    `alias=...` identical to the `alias` set previously for the highlight
    in question; you only need to provide an alias to a highlight
    if you first turn it on and want to turn it off/modify later.
    + To modify the properties of a previously created highlight, use
    `mode=n` along with property modifiers such as `thickness=...`,
    `isovalue=...` or `alpha=...`. To change the property in a smooth
    manner, use the colon-based (`...=init_value:end_value`) range syntax.
    + Highlights can also be created or deleted instantaneously (e.g.
    at the beginning of a scene instead of adding a VMD representation)
    if the `t=...s` keyword is omitted *and* `mode=u/d` is specified.
    + With `style=isosurface`, volumetric data (e.g., CCP4 density maps
    or electron densities) can be shown, given that the mandatory 
    keyword `isovalue=...` is provided. When a range of values is given
    (e.g., `isovalue=0.01:0.2`), the isovalue will change continuously.
    Note: `molywood` does **not** normalize the isovalues, so that one 
    should use the same values as used in VMD.
    + The semi-transparent fade-in and fade-out of highlights can be
    controlled with the `fade_in=...` and `fade_out=...` keywords,
    which specify the fraction (from 0 to 1) of the action's duration
    spent at the transition. E.g., when `mode=ud` (default), `t=4s`, 
    `fade_in=0` and `fade_out=0.5`, the highlight will appear instantaneously,
    will be opaque for 2s and will disappear smoothly over the next 2s.
    `cutoff=...` provides a cut-off value for the Dynamic Bonds selection,
    only relevant when `style=dynamicbonds` is specified.
    + To differentiate between selections, `color=name` yields cyan carbon
    atoms, `color=type` yellow and `color=element` black. This can be 
    overridden using the syntax e.g. `color=type-C:green-H:black` (please
    note that this is a global setting, i.e. affects all representations
    colored by type/name/element).
    + To show an entire ensemble (or dynamically transition between a single
    structure and an ensemble of structures), use the `multiframe=...` 
    parameter. Acceptable values are a single integer, a range of
    integers (e.g. `0-10`), a range of integers with stride (e.g. `0-10-2`),
    or transition between two ranges (e.g.`0:0-10` goes smoothly from showing 
    only frame 0 to showing frames 0 through 10; remember frame indices are
    0-based in VMD). Use `multiframe=now` to go back to showing the current 
    frame if needed.
    + To add a contour to a material, use `contour=X` where X is a number between
    0 (no contour) and 1 (thick contour). This feature uses the 
    Outline/OutlineWidth feature of VMD's Materials. You can also modify it 
    on-the-fly using the range syntax, e.g. `contour=0:1`.
    + To make a dynamic *selection*, place a '[]' in the `selection` text,
    and provide a value or value range for `inline_parameter` - the value will
    be dynamically inserted in the specified placeholder. For example, one can
    choose `selection="resid 1 to []"` and `inline_parameter=2:100`, making the
    selection change dynamically from "resid 1 to 2" to "resid 1 to 100";
    alternatively, this can be applied to geometric selections, such as
    `selection="z > []"` or `selection="within [] of index 10"`. **NOTE**: if the
    selection has to be an integer (e.g. for indices, residue numbers etc.),
    supply `inline_parameter` with integers, i.e. without the decimal separator.
    For example, `inline_parameter=1:10` will yield integers, while 
    `inline_parameter=1.0:10.0` will interpolate using floats.
    + If multiple molecules are present in the system/Scene, `highlight`
    by default creates an identical representation for all molecules. To
    specifically add representations for selected molecule(s), use
    `molecules=mol_id`, where `mol_id` is a 0-based index of the molecule,
    or a comma- (but not space-) separated list of indices of molecules.
    In past versions, this behavior could be partially emulated using
    `toggle_molecule`; now this old way is obsolete.
    + If .cube files are selected, you can use the `isovalue` keyword to set
    the desired density isovalue, either as a static or dynamic quantity
    (e.g. `isovalue=0.1:0.3` to make it change from 0.1 to 0.3 during the 
    action). 
    + Isovalues can optionally be colored with other density data;
    to do this, load simultaneously cubes for isovalue rendering and coloring,
    use `color=volume:lower_bound:upper_bound` to set the color scale bounds,
    and set `volume_offset=N` so that frame `i` will be colored according to
    density from frame `i+N`.
+ `restore_viewpoint` returns to a viewpoint (defined as a camera position,
with a rotation/traslation/zoom values) saved previously with `save_viewpoint`
by referencing the respective `alias`. In principle, the same result could be
achieved by doing all the respective operations in reverse order, but this option
provides a more convenient way of achieving this. Can be combined with `fraction`,
`sigmoid` and `abruptness` if needed.
+ `make_transparent`/`make_opaque` change the opacity of a selected
`material` to make it fully transparent or fully opaque in time `t`.
`start` and `limit` optionally accept floating-point values from 0 to 1
to restrict the transition to this range (note that transparency=0 is
equivalent to opacity=1). `sigmoid` and `abruptness` work like for
`rotation`. When `t` is not specified, the change in opacity is
instantaneous, and `start` and `limit` can be used interchangeably.
    + `make_transparent material=Opaque` can be used with default VMD
    representations to make them disappear completely, and then new
    representations can be added using `highlight` from scratch.

<a name="notes-finite"></a>
##### Finite-time actions:

+ `show_figure` just shows an image instead of a VMD render during time
`t`; the image is specified using `figure=fig_file_name`. Optionally,
a matplotlib-based plot can be generated when `datafile` is specified
(see the respective options in `add_overlay` for reference).
+ `do_nothing` renders a still VMD scene for time `t` without doing
anything else (although can be combined e.g. with `add_overlay`).
+ `add_overlay` allows to add an inset to the scene, with the position
of the inset's bottom left corner specified through `origin` (0,0
corresponds to the bottom left and 1,1 to the upper right corner of the
figure, as in a regular Cartesian coordinate system), and size specified
through `relative_size` (1.0 means fit into the whole scene, 0.25 means
fit into a rectangle 25% of the scene size). Independently, the aspect
ratio (X to Y size) can be specified using `aspect_ratio=...`. 
    + In the Molywood GUI, overlays will be displayed during preview
    as squares (scenes, figures, graphs) or crosses (text) to indicate
    the placement of the overlay, including their dynamic movement.
    The indicated placement of text will take into account text length 
    and line breaks, but not e.g. LaTeX characters. The placeholders
    for figures indicate the area but not necessarily the aspect ratio
    (it's properly calculated for scenes and graphs though).
    + The `show_grid` action allows to dump a .png image with a regular 
    10x10 grid overlaid to facilitate the placement of overlays.
    + The content of the overlay can be: an external figure (specified
    through `figure`); an on-the-fly  generated matplotlib 1D/2D
    plot (based on a data file speficied with `datafile`);
    plain text (specified through `text`); another scene (specified
    through `scene`); or an external movie (specified through `movie`).
    + If `datafile=...` is specified, a dot can dynamically follow the
    values on the plot. By default, the script will try to use `frames`
    from the accompanying `animate` action to select datapoint indices
    from the `datafile`. To independently select datapoint indices for
    the 1D/2D plot (e.g. when the `datafile` has much more entries than
    the trajectory used in `animate`, one can supply `datafile=...`
    with `dataframes=init_frame:final_frame` - it will take precendence
    over `animate`'s `frames`. If `dataframes` cannot be written as a 
    simple range, write frame numbers to a file and reference it using
    `dataframes_from_file=...`    
    + If the datafile starts with a single line formatted as
    `# x axis label; y axis label`, `x axis label` and `y axis label`
    will be used to label the corresponding axes of the plot.
    + If the datafile contains a line starting with `!`, it can specify
    a number of matplotlib-compatible `key=value` pairs to modify the
    properties of the plot, e.g. `color='C1' ylim=[-2,2] ls='--'` sets
    the color as orange, with a dashed line, and in Y-range from -2 to 2.
    + If the datafile has more than two (N) columns, data will be plotted
    as multiple (N-1) 1D lines.
    + Currently, 1D line plots (generated with plt.plot) and
    2D histograms (generated with plt.hexbin) are supported; the former
    is default if a `datafile` is supplied, and the latter can be
    requested with `2D=t`. With `seaborn=t`, the 2D plot is rendered using
    the pretty-looking Seaborn library.
    + If `text="sample text"` is used, the text will be displayed at
    a position specified with `origin`. Text properties can then be
    set using `textsize` and `textcolor`.
    + A nice rounded box around the text can be added by setting
    `textbox=t`. Alternatively, box properties can be specified in place
    of `t` following the [matplotlib documentation](https://matplotlib.org/3.2.1/api/_as_gen/matplotlib.patches.FancyBboxPatch.html#matplotlib.patches.FancyBboxPatch), and setting e.g.
    `textbox=boxstyle:rarrow,ec:r` will yield the box shaped as a 
    right-pointing arrow, with edge color set to red.
    + To use "dynamic" numbers in text overlays, use `[]` with
    `dataframes=init_value:final_value` - `[]` will be substituted with
    the appropriate (linearly interpolated) number. For rounding use
    `decimal_points=...` (by default, 3 decimal points are shown).
    + When a valid scene name is specified in `scene=...`, another scene
    will be rendered and used as an overlay.
    + Providing an external file `movie=video.mp4` will disassemble the
    provided movie into frames and re-compose them into the current scene.
    + Mulitple overlays can be added to a scene simultaneously; adding
    many `add_overlay` commands separated by semicolons and encircled
    in curly brackets works just as any other multiple action (see
    `Notes on input formatting` above).
    + The default setting is `mode=ud`, in analogy to `highlight`; if `mode=u`,
    the overlay will remain in place until the end of the movie,
    unless `mode=d` is specified later with a matching `alias=...`
    entry.
    + By specifying `alpha`, the overlay can be made semi-transparent;
    `alpha` can either be a float between 0 and 1 (e.g. `alpha=0.3`),
    or a range separated by a colon (e.g. `alpha=1:0`); in the latter
    case, transparency will change linearly from the first value
    (here non-transparent, alpha=1) to the other (here fully
    transparent, alpha=0).
    + To modify the properties of a previously created overlay, use
    `mode=n` along with property modifiers such as `origin=...`,
    `relative_size=...` or `alpha=...`. To change the property in a smooth
    manner, use the colon-based (`...=init_value:end_value`) range syntax. 
    To indicate which overlay will be affected, the `alias` keyword has to
    be specified both initially (with `mode=u`) and in the current overlay (`mode=n`).
    + Setting `transparent_background=t` will convert all white
    pixels in the overlay into transparent ones; this way, overlays can
    be easily composed one upon another without covering the content.
    + To use images' **centers** instead of lower-left corners for 
    alignment, use `center=t`. In this way, the default `origin=0,0` will
    place the overlay's center exactly in the middle of the Scene, which might be 
    a desired outcome e.g. for scene-wide text labels that don't have a
    predictable size. Similarly, `origin=0,-0.4` will put the overlay's 
    center at the middle of the Scene horizontally, and at 10% of the 
    Scene's height vertically.
+ `add_audio` allows to add a soundtrack to the movie, be it a piece of
music or a spoken comment. Like `add_overlay`, it has to be combined
with another action using the curly bracket `{...}` notation. By default,
a given recording starts at the beginning of the action, and can exceed
the action's duration, even until the end of the movie.
    + The audio file can be specified with the `audiofile=...` keyword;
    for now, only the .mp3 format is properly tested.
    + When `from=...` and `length=...` are specified, the input audio
    will be trimmed to start from the specified timestamp and have the 
    appropriate duration (both values are in seconds and can be floats).
    + Within a movie, many audio files can be added, although only one
    file per action is supported. If consecutive audio files overlap,
    the preceding one will be interrupted where the next one begins.
    + To adjust the volume of individual audio segments, add 
    `volume=...`, passing the new volume relative to the original
    (a value of `1` yields the original volume)

<a name="notes-extra"></a>
### Notes on extra graphics features

+ External figures (e.g. ending credits) can be featured in the movies.
The external graphics file has to be specified as a parameter in the
`show_figure` action (e.g. `show_figure t=4s figure=picture.png`).
+ Note that it is *the user* who is responsible for setting correct
resolutions for the individual scenes. By default, each scene is
rendered in a resolution of 1000x1000; if e.g. a source figure isn't
exactly rectangular, it will be scaled to
fit in the rectangle, but its aspect ratio (shape) will not be affected.
+ When generating figures with matplotlib, axis labels can be specified
directly in the datafile by adding a `# x label; y label` (spaces and
latex-compatible math notation are allowed). In addition, multiple
matplotlib-compatible `keyword=value` pairs can be enumerated after the
`!` character to modify plt.plot/plt.hexbin defaults, e.g.
`! bins='log' cmap='seismic'`, or plot properties, e.g.
`! xlim=-1,2 ylim=0,500` (no spaces around the `=` sign).
+ Invidivual scenes can be combined not only side-by-side (as using
`$ layout ...`), but also one after the other. To append `next_scene`
at the end of `previous_scene`, it is sufficient to set e.g.
`$ next_scene visualization=... after=previous_scene`. Note that to
avoid technical issues, it is advised to list the scenes in the input
in the actual order they need to be concatenated, especially when
concatenating more than two of them.

<a name="gui"></a>
### Molywood GUI in VMD

Starting from VMD 2.0, VMD features a GUI that allows to compose videos
directly in a VMD session using Molywood syntax and concepts. It: 
+ allows for fast live preview of individual scenes, 
+ eliminates the need to look up the documentation for keywords, 
+ features a templates library that covers the most typical use cases,
+ enables the user to launch rendering directly from the GUI, or generate an input file to execute on an external machine,
+ displays a movie diagram viewer makes the design of overlays and multi-scene videos more intuitive, 
+ integrates several new features (e.g. viewpoint saving/restoring) between VMD and Molywood.

Many thanks to **Diego E.B. Gomes**, **Barry Isralewitz**, and **Emad 
Tajkhorshid** for the technical and conceptual help in developing the GUI!

<a name="issues"></a>
### Known issues

+ VMD labels are always rendered using the `Opaque` material, so that
making this material transparent (`make_transparent material=Opaque`)
will make the labels disappear as well; the straightforward solution is
to use a different material, or generate a copy.
(`VMD Main > Graphics > Materials > Create New`)
+ The labels are rendered with a slightly different size and
appearance with the Snapshot (`draft=t`) and Tachyon (`draft=f`)
renderers.
+ `fit_trajectory` combined with `axis` can become slow if combined
with large `selection` (in terms of number of atoms) and `animation`
with large `smooth=...` values; it is suggested to only use backbones
for alignment to principal axes, as the repeated calculation of tensors
of inertia can be time-consuming in VMD.
+ With the default `ffmpeg` settings, choppy video playback has been
reported when VLC is being run on OSX; this is solely a video player
issue.
+ In the `draft=t` mode, setting the frame resolution higher than
screen resolution will limit the frame's dimension, possibly interfering
with the positioning of overlays (e.g. `resolution=1500,1500` will
not work properly on a 1280x1024 screen). This can be an issue particularly
for users of the i3 window manager on Linux. However, things will work
properly when the draft mode is turned off.
+ Due to how Tachyon renders transparencies, at times the default setting
`max_transparent_surfaces=1` might turn out troublesome when working with
many transparent representations overlaid on one another: if that is the
case, try increasing this value. The default is set to 3 as otherwise
representations like Licorice or NewCartoon look quite ugly when transparent.
+ When trajectories are loaded along with a .gro file, VMD can throw 
segmentation faults; this can be fixed by converting the .gro to .pdb.
+ When two highlights coincide exactly (i.e. have the same `style` and 
`thickness`) and one of them is made fully transparent, the other can 
disappear too, due to how Tachyon handles transparencies; this can be 
solved by changing the `thickness` of one of them very slightly, e.g.
from `1` to `1.05`.
+ In text overlays, newlines (`\n`) are currently incompatible with 
backslash-dependent TeX characters (like `\alpha`)

<a name="tips"></a>
### Advanced tips

+ To inspect the TCL code directly, you can add the `-test` keyword when running
molywood (`molywood script.txt -test`), and in this way the TCL code will
be generated only but not executed in VMD.
+ To insert a custom modification to the TCL code on-the-fly, use `insert_tcl`
with `file=...` pointing to a text file with a TCL snippet, or `code=...` to define
the code (preferably a one-liner) directly in the input file. You can use VMD's
`VMD Main > File > Log TCL Commands to...` to find out how to express the desired
action as a TCL command.
+ To insert a TCL snipped into a loop using `insert_tcl`, making the custom command 
dynamic/variable, use `loop_command`
to specify the command, putting an empty curly bracket (`{}`) where the variable should
go. Then, either specify a range of values for the loop with `range=init_val:final_val`
or list values (if have to be non-linear) using `loopover=val1,val2,val3,...`.
Number of values can be different from the number of frames, this will be adjusted.
Note that for this feature to take effect, `insert_tcl` has to be combined with another
action.
+ To split a movie into several shorter movies (e.g. to put them into individual 
slides), add the global `breakpoints=t1,t2,t3,...` option, where t1, t2, t3, ... 
are times (in seconds) at which the movie will be broken into subparts.
+ To export your movie as a .gif (on top of having the .mp4), use the global option
`gif=fps,width_px`, where fps is the frame-per-second of the resulting gif, and 
width_px is the gif width in pixels (defaults are: 8 fps & 400 px). Through quality 
reduction, gifs can be more easily embedded in websites or other online resources.`