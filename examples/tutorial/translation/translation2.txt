$ global name=translation2
$ scene1 visualization=../TransAtlas.vmd 
$ scene2 visualization=../TransAtlas.vmd after=scene1

! two scenes can be combined one after another
! to create an impression of merging or switching

# scene1
animate    frames=0
do_nothing t=1s
translate  vector=-3,0,0 t=1s fraction=:0.5


# scene2
animate    frames=17
translate  vector=1.5,0,0
translate  vector=-3,0,0 t=1s fraction=0.5:
do_nothing t=2s
