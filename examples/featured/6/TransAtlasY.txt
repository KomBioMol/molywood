$ global 
$ main_scene     visualization=TransAtlas3.vmd resolution=1500,1000
$ scene1         visualization=TransAtlas3.vmd resolution=1500,1000
$ scene2         visualization=TransAtlas3.vmd resolution=1500,1000
$ scene3         visualization=TransAtlas3.vmd resolution=1500,1000


# main_scene,scene1,scene2,scene3

    ! We make the same translation for all scenes:

translate     vector=0.5,0,0


# main_scene
do_nothing    t=1s

    ! We start from the closed state (frame=17) and animate the transition backwards,
    ! adding an overlay for the "static" closed state (scene1):

{
 animate      t=2s frames=17:9;
 add_overlay  scene=scene1 transparent_background=t mode=u
}

    ! At the midpoint, we add another overlay (scene2) for the intermediate state, 
    ! a text label for the previous overlay, and continue the animation:

{
 animate      t=2s frames=8:0;
 add_overlay  scene=scene2 transparent_background=t mode=u;
 add_overlay  text=closed origin=0.25,0.75 mode=u
}

    ! As we reach the open state, we add yet another structural overlay (scene3), 
    ! another text label, and animate to the initial state:

{
 animate      t=2s frames=0:17;
 add_overlay  scene=scene3 transparent_background=t mode=u;
 add_overlay  text=intermediate origin=0.25,0.4 mode=u
}

    ! Here we add the last text label:

{
 do_nothing    t=2s;
 add_overlay  text=open origin=0.25,0.05 mode=u
}


# scene1

    ! This is a still frame in the closed state (frame=17), but we start fully-sized
    ! on the right hand size, and then use zoom_out and translate to move to the
    ! appropriate location (here upper left corner):

animate       frames=17
{
 zoom_out     scale=2.5;
 translate    vector=-2.5,1.2,0 t=1s
}
do_nothing    t=7s


# scene2                       
animate       frames=8

    ! Same as above, but we just move to the left:

{
 zoom_out     scale=2.5;
 translate     vector=-2.5,0.2,0 t=1s
}
do_nothing    t=5s


# scene3
animate       frames=0

    ! ...and the bottom left corner:

{
 zoom_out     scale=2.5;
 translate     vector=-2.5,-0.8,0 t=1s
}
do_nothing    t=3s

