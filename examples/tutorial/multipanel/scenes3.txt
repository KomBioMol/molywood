$ global name=scenes3
$ scene1 pdb_code=1w0t,1w0u

# scene1

! It is possible to load two structures at once
! (also using structure=... and trajectory=...,
! or having two molecules in the VMD visualization)
! and then use `toggle_molecule` to e.g. freeze
! selected representations, only moving other ones

rotate  axis=x angle=90
translate vector=0.4,0,0
toggle_molecule molecule_id=1 freeze=t
translate vector=-2.5,-1.8,0
translate vector=5,0,0 t=2s fraction=0:0.5
toggle_molecule molecule_id=1 freeze=f
toggle_molecule molecule_id=0 freeze=t
translate vector=5,0,0 t=2s fraction=0.5:1

