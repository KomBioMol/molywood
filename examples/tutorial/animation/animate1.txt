$ global name=animate1
$ scene1 visualization=../TransAtlas.vmd 

# scene1

! one value instantaneously jumps to frame,
! while a range shows animated trajectory;
! smooth applies smoothing to all representations

animate  frames=0
animate  frames=0:17  t=2s
animate  frames=17:0  t=2s smooth=5
